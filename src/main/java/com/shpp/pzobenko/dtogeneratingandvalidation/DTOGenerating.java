package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.dto.DataTransferObject;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class DTOGenerating {
    private DTOGenerating() {
    }

    /**
     * Create stream which generating dto by random values and adding in the array
     *
     * @return array with dto
     */
    public static DataTransferObject[] generate() {
        DataTransferObject[] dtoList = new DataTransferObject[Constants.NUMBER_OF_GOODS_PER_STREAM];
        AtomicInteger count = new AtomicInteger();
        Stream.generate(() -> generateDto(new DataTransferObject())).
                limit(Constants.NUMBER_OF_GOODS_PER_STREAM).forEach(d -> dtoList[count.getAndIncrement()] = d);
        return dtoList;
    }

    /**
     * Create dto with random values
     *
     * @param dto needs to don`t create DataTransferObject each time
     * @return dto
     */
    public static DataTransferObject generateDto(DataTransferObject dto) {
        dto.setCategoryId(ThreadLocalRandom.current().nextInt(0, Constants.CATEGORY_SIZE + 1));
        dto.setAddressOfShop(ThreadLocalRandom.current().nextInt(0, Constants.ADDRESS_SIZE + 1));
        StringBuilder builder = new StringBuilder();
        builder.append("'");
        for (int i = 0; i < ThreadLocalRandom.current().nextInt(1, Constants.LENGTH_OF_GOODS_NAME); i++) {
            builder.append(Constants.getLetters()[ThreadLocalRandom.current().nextInt(Constants.getLetters().length)]);
        }
        builder.append("'");
        dto.setNameOfGood(builder.toString() );
        return dto;
    }
}
