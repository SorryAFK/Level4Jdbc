package com.shpp.pzobenko.constandprop;

public class Constants {
    /**
     * Empty constructor.
     */
    private Constants() {
    }

    /**
     * Num of DTOs to send on tables products
     */
    public static final int NUM_OF_DTO = 3000000;
    /**
     * Length of goods name
     */
    public static final int LENGTH_OF_GOODS_NAME = 20;
    /**
     * Number of goods per stream
     */
    public static final int NUMBER_OF_GOODS_PER_STREAM = 10000;
    /**
     * Name of epicenter `table
     */
    public static final String NAME_OF_EPICENTER_TABLE = "epicenter ";
    /**
     * Name of categories table
     */
    public static final String NAME_OF_CATEGORIES_TABLE = "categories";
    /**
     * Two string which helps get num of the category from user by every shop;
     */
    public static final String SELECT_MESSAGE1 = "SELECT COUNT(products.id_epicenter) FROM products " +
            "JOIN categories c on c.id_category = products.id_category " +
            "JOIN epicenter e on e.id_epicenter = products.id_epicenter WHERE c.name = '";

    public static final String SELECT_MESSAGE2 = "' AND e.address = ";
    /**
     * Category array size
     */
    public static final int CATEGORY_SIZE = 19;
    /**
     * Address array size
     */
    public static final int ADDRESS_SIZE = 10;
    /**
     * Array with addresses in Kiev
     */
    private static final String[] ADDRESS = new String[]{"'Vulytsya Berkovetska 6B'", "'Nove Hwy 48'",
            "'Polyarna St 20D'", "'Stepana Bandery Ave 36А'", "'Bratyslavska St 11'", "'Kyivska St 253'", "'Kiltseva Rd 1B'",
            "'Petra Hryhorenka Ave 40'", "'Mahistralna St 2'", "'Vulytsya Horbatyuka 2'"};
    /**
     * Some real categories from epicenter
     */
    private static final String[] CATEGORY = new String[]{"'Repairs'", "'Floor covering'", "'Plumbing'",
            "'Garden and vegetable garden'", "'Pet products'", "'Illumination'", "'For home'", "'Tools'", "'Household appliances'",
            "'Electronics'", "'Children s goods'", "'Beauty and health'", "'Products'", "'Clothes, shoes, accessories'",
            "'Sports goods'", "'Tourism and the military'", "'Household chemicals'", "'For cars'", "'Stationery'"};
    /**
     * Letter chars array which helps generates names for goods
     */
    private static final char[] LETTERS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',};
    /**
     * Three strings which needs to create tables and insert values on it
     */
    public static final String INSERT_INTO = "INSERT INTO ";
    public static final String VALUES = " VALUES";
    public static final String PRODUCTS_TABLE = "INSERT INTO products VALUES";

    /**
     * get letters needs to secure array letters from changes
     *
     * @return letters array
     */
    public static char[] getLetters() {
        return LETTERS;
    }

    /**
     * get address needs to secure array address from changes
     *
     * @return address array
     */
    public static String[] getAddress() {
        return ADDRESS;
    }

    /**
     * get Category needs to secure array Category from changes
     *
     * @return Category array
     */
    public static String[] getCategory() {
        return CATEGORY;
    }
}