package com.shpp.pzobenko.jdbc;


import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.constandprop.Prop;
import com.shpp.pzobenko.stopwatch.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.shpp.pzobenko.dtogeneratingandvalidation.SQLCommandWriter.*;


public class Connection {
    private static final Logger log = LoggerFactory.getLogger(Connection.class);

    private Connection() {
    }

    public static void connection(Prop prop, String categoryName) {
        Statement statement = null;
        java.sql.Connection connection = null;
        StopWatch stopWatch = new StopWatch();
        try {
            // Register JDBC driver
            Class.forName(prop.getDriver());

            // Open a connection
            log.info("Connecting to database...");
            connection = DriverManager.getConnection(prop.getUrl(), prop.getUsername(), prop.getPassword());
            // Execute a query
            log.info("Creating statement...");
            statement = connection.createStatement();
            statement.execute(createTables(prop));

            log.info("Start generating dto");
            StringBuilder stringBuilder = new StringBuilder();
            AtomicInteger count = new AtomicInteger();
            while (count.get() * Constants.NUMBER_OF_GOODS_PER_STREAM != Constants.NUM_OF_DTO) {
                statement.addBatch(createInsertCommands(stringBuilder, count));
                if (count.get() % 100 == 0) {
                    statement.executeBatch();
                }
                stringBuilder.replace(0, stringBuilder.length(), "");
                count.incrementAndGet();
            }
            statement.executeBatch();
            log.info("Table products create successful and fuel by random value");
            log.info("Time of creating goods db {}",stopWatch.getTimeInSeconds());
            statement.execute(prop.getDdlScriptIndex());
            statement.execute(prop.getDdlScriptAlter());
            stopWatch = new StopWatch();
            //Reading results of selects and putting result in to hashmap
            HashMap<String, Integer> getResultOfSelect = new HashMap<>();
            int countForAddresses = 0;
            for(String select : selectMaxValueByCategory(categoryName)) {
                ResultSet rs = statement.executeQuery(select);
                String[] addresses = Constants.getAddress();
                while (rs.next()) {
                    getResultOfSelect.put(addresses[countForAddresses], rs.getInt("count"));
                }
                countForAddresses++;
            }
            log.info("The big num of category {}", categoryName);
            String res = findBigNum(getResultOfSelect);
            log.info("is in the store by address {}", res);
            log.info("Time of getting big store {}", stopWatch.getTimeInMSeconds());
            statement.close();
            connection.close();
        } catch (SQLException | ClassNotFoundException se) {
            log.error("Boom! {0}" , se);
        } finally {
            // finally block used to close resources
            try {
                if (statement != null) statement.close();
            } catch (SQLException se2) {
                log.error("Boom! {0}" , se2);
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    private static String findBigNum(HashMap<String, Integer> getResultOfSelect) {
        int findBiggest = 0;
        String result = null;
        int getResultOfSelectGet;
        for (Map.Entry<String, Integer> address : getResultOfSelect.entrySet()) {
            getResultOfSelectGet = address.getValue();
            if(findBiggest <  getResultOfSelectGet) {
                result = address.getKey();
                findBiggest = getResultOfSelectGet;
            }
        }
        return result;
    }
}