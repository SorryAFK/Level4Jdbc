package com.shpp.pzobenko.validator.nameofgoods;

import com.shpp.pzobenko.constandprop.Constants;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidateDTONameOfGoods implements ConstraintValidator<ValidateDTOName,String> {
    @Override
    public void initialize(ValidateDTOName constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return  s.length() < Constants.LENGTH_OF_GOODS_NAME && !s.isEmpty();
    }
}

