package com.shpp.pzobenko.validator.addresses;

import com.shpp.pzobenko.constandprop.Constants;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ValidateDTOAddressOfGoods.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateDTOAddress {
    String message() default "The column 'address' have size more than " + Constants.ADDRESS_SIZE + " or equals 0";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
