package com.shpp.pzobenko.validator.addresses;


import com.shpp.pzobenko.constandprop.Constants;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidateDTOAddressOfGoods implements ConstraintValidator<ValidateDTOAddress, Integer> {
    @Override
    public void initialize(ValidateDTOAddress constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Integer i, ConstraintValidatorContext constraintValidatorContext) {
        return i <= Constants.ADDRESS_SIZE && i != 0;
    }
}