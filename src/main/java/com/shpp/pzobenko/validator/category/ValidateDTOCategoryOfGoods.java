package com.shpp.pzobenko.validator.category;

import com.shpp.pzobenko.constandprop.Constants;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidateDTOCategoryOfGoods implements ConstraintValidator<ValidateDTOCategory, Integer> {
    @Override
    public void initialize(ValidateDTOCategory constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Integer i, ConstraintValidatorContext constraintValidatorContext) {
        return i <= Constants.CATEGORY_SIZE && i != 0;
    }
}