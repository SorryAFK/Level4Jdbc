package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.constandprop.Prop;
import com.shpp.pzobenko.dto.DataTransferObject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class SQLCommandWriterTest {


    @Test
    void selectMaxValueByCategory() {
        List<String> expected = new ArrayList<>();
        StringBuilder buildSelect = new StringBuilder();
        for (String address : Constants.getAddress()) {
            buildSelect.append(Constants.SELECT_MESSAGE1).append("Tourism and the military").
                    append(Constants.SELECT_MESSAGE2).append(address).append(";");
            expected.add(buildSelect.toString());
            buildSelect.replace(0, buildSelect.length(), "");
        }
        assertEquals(expected,SQLCommandWriter.selectMaxValueByCategory("Tourism and the military"));
    }

    @Test
    void createInsertCommands() {
        assertNotNull(SQLCommandWriter.createInsertCommands(new StringBuilder(),new AtomicInteger()));
    }
}