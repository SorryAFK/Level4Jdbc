package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.dto.DataTransferObject;
import org.junit.jupiter.api.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.junit.jupiter.api.Assertions.*;

class DTOValidateTest {

    @Test
    void validate() {
        DataTransferObject[] dtoWithNonValidValues = new DataTransferObject[10];
        DataTransferObject dto = new DataTransferObject();
        for (int i = 0; i <  dtoWithNonValidValues.length; i++) {
            dtoWithNonValidValues[i] = dto;
            dtoWithNonValidValues[i].setAddressOfShop(0);
            dtoWithNonValidValues[i].setCategoryId(0);
            dtoWithNonValidValues[i].setNameOfGood("");
        }
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        for (DataTransferObject dtoWithNonValidValue : dtoWithNonValidValues) {
            assertFalse(validator.validate(dtoWithNonValidValue).isEmpty());
        }
        DTOValidate.validate(dtoWithNonValidValues);
        for (DataTransferObject dtoWithNonValidValue : dtoWithNonValidValues) {
            assertTrue(validator.validate(dtoWithNonValidValue).isEmpty());
        }
    }
}