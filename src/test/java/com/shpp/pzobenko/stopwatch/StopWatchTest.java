package com.shpp.pzobenko.stopwatch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StopWatchTest {

    @Test
    void getTimeInSeconds() {
        long expected = System.currentTimeMillis();
        StopWatch test = new StopWatch();
        assertEquals((expected - System.currentTimeMillis()) / 1000, test.getTimeInSeconds());
    }
}